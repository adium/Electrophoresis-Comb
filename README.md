# Electrophoresis-Comb
Customizable DNA comb for Bio-Rad Horizontal Electrophoresis Systems

Designed using [OpenSCAD](http://www.openscad.org/), a free program for creating solid 3D CAD objects.  The 3D model is generated using numeric values, allowing anyone to customize it and print their own custom combs.  (No more tape to make larger wells!)

#### STL Preview
![8 tooth comb](https://github.com/admish/Electrophoresis-Comb/blob/master/img/stl_preview.jpg)

The preset model is specifically designed to work with Thingiverse's customizer tool.  This allows the non-technical user to modify this model and make their own STL file for printing.

[View on Thingiverse](http://www.thingiverse.com/thing:1346188)

#### Thingiverse Customizer
![Thingiverse Customizer](https://github.com/admish/Electrophoresis-Comb/blob/master/img/thingiverse_customizer.png)

